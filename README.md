#Demo SideScrollingGame

##Based on rwtutorial 
[Tutorial link](https://www.raywenderlich.com/62049/sprite-kit-tutorial-make-platform-game-like-super-mario-brothers-part-1). 

###used 
[Tile editor](http://www.mapeditor.org (https://github.com/bjorn/tiled)) - to generate tiles

###demo 
![gif](demo/demo.gif)